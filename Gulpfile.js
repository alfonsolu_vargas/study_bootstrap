// File: Gulpfile.js
'use strict';

var nib = require('nib'),
   gulp = require('gulp'),
   gulpif = require('gulp-if'),
   uncss = require('gulp-uncss'),
   inject = require('gulp-inject'),
   jshint = require('gulp-jshint'),
   stylus = require('gulp-stylus'),
   uglify = require('gulp-uglify'),
   useref = require('gulp-useref'),
   connect = require('gulp-connect'),
   stylish = require('jshint-stylish'),
   wiredep = require('wiredep').stream,
   minifyCss = require('gulp-minify-css'),
   templateCache = require('gulp-angular-templatecache'),
   historyApiFallback = require('connect-history-api-fallback');

// Servidor web de desarrollo
gulp.task('server', function() {
   connect.server({
      root: './app',
      hostname: '0.0.0.0',
      port: 8081,
      livereload: true//,

      /* error si se des-comentarea
         middleware: function(connect, opt) {
            //return [ historyApiFallback ];
      //}
      */

   });
});


gulp.task('server-dist', function() {
   connect.server({
      root: './dist',
      hostname: '0.0.0.0',
      port: 8081,
      livereload: true
      /*
         middleware: function(connect, opt) {
            return [ historyApiFallback ];
         }
      */
   });
});
// Busca errores en el JS y los muestra por pantalla
gulp.task('jshint', function() {
   return gulp.src('./app/scripts/**/*.js')
   .pipe(jshint('.jshintrc'))
   .pipe(jshint.reporter(stylish))
   .pipe(jshint.reporter('fail'));
});


// Preprocesa archivos Stylus a CSS y recarga los cambios
gulp.task('css', function() {
   gulp.src('./app/stylesheets/main.styl')
   .pipe(stylus({ use: nib() }))
   .pipe(gulp.dest('./app/stylesheets'))
   .pipe(connect.reload());
});


// Recarga el navegador cuando hay cambios en el HTML
gulp.task('html', function() {
   gulp.src('./app/**/*.html')
   .pipe(connect.reload());
});


// Busca en las carpetas de estilos y javascript los archivos que hayamos creado
// para inyectarlos en el index.html
gulp.task('inject', function() {
   var sources = gulp.src(['./app/scripts/**/*.js','./app/stylesheets/**/*.css','./app/lib/**/*.css']);
   return gulp.src('index.html', {cwd: './app'})
   .pipe(inject(sources, {
      read: false,
      ignorePath: '/app'
   }))
   .pipe(gulp.dest('./app'));
});


// Inyecta las librerias que instalemos vía Bower
gulp.task('wiredep', function () {
   gulp.src('./app/index.html')
   .pipe(wiredep({
      directory: './app/lib'
   }))
   .pipe(gulp.dest('./app'));
});


//optimizando codigo para producción.Cacheado de plantillas HTML
var templateCache = require('gulp-angular-templatecache');
gulp.task('templates', function() {gulp.src('./app/views/**/*.tpl.html')
   .pipe(templateCache({
      root: 'views/',
      module: 'blog.templates',
      standalone: true
   }))
   .pipe(gulp.dest('./app/scripts'));
});


//optimizando codigo para producción.
//Tarea que se encarga de minificar y ficheros JS y CSS
gulp.task('compress', function() {
   gulp.src('./app/index.html')
   .pipe(useref())
   .pipe(gulpif('*.js', uglify({mangle: false })))
   .pipe(gulpif('*.css', minifyCss()))
   .pipe(gulp.dest('./dist'));
});


//optimizando codigo para producción.
//Minificado de ficheros index.html pero sin comentarios con enlaces a los nuevos fichero.
gulp.task('copy', function() {
   gulp.src('./app/index.html')
   .pipe(useref())
   .pipe(gulp.dest('./dist'));
   gulp.src('./app/lib/fontawesome/fonts/**')
   .pipe(gulp.dest('./dist/fonts'));
});


//Crea servicio minificado del proyecto
gulp.task('uncss', function() {
   gulp.src('./dist/css/style.min.css')
   .pipe(uncss({
      html: ['./app/index.html', './app/views/post-detail.tpl.html', './app/views/post-list.tpl.html']
   }))
   .pipe(gulp.dest('./dist/css'));
});


// Vigila cambios que se produzcan en el código y lanza las tareas relacionadas
gulp.task('watch', function() {
   gulp.watch(['./app/**/*.html'], ['html']);
   gulp.watch(['./bower.json'], ['wiredep']);
   gulp.watch(['./app/scripts/**/*.js'], ['jshint']);
   gulp.watch(['./app/stylesheets/**/*.styl'], ['css']);
   gulp.watch(['./app/stylesheets/**/*.styl'], ['css', 'inject']);
   gulp.watch(['./app/scripts/**/*.js', './Gulpfile.js'], ['jshint', 'inject']);
});

//Funciona ejecutando Unicamente $ gulp
gulp.task('default', ['server', 'inject', 'wiredep', 'watch']);
gulp.task('build', ['templates', 'compress', 'copy', 'uncss']);


/*

funciona para ejecutar $ gulp build
gulp.task('build', ['templates', 'compress', 'copy']);
gulp.task('default', ['server', 'inject', 'wiredep', 'watch']);

Funciona con el comando $ gulp server-dist
gulp.task('build', ['templates', 'compress', 'copy', 'uncss']);
*/
